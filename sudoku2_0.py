import random


def read_sudoku(filename):
    digits = [c for c in open(filename).read() if c in '123456789.']
    grid = group(digits, 9)
    return grid


def display(values):
    width = 2
    line = '+'.join(['-' * (width * 3)] * 3)
    for row in range(9):
        print(''.join(values[row][col].center(width) + ('|' if str(col) in '25' else '') for col in range(9)))
        if str(row) in '25':
            print(line)
    print()


def group(values, n):
    return [[values[i * n + j] for j in range(n)] for i in range(n)]


def get_row(values, pos):
    return values[pos[0]]


def get_col(values, pos):
    return [values[i][pos[1]] for i in range(len(values))]


def get_block(values, pos):
    row, col = pos
    row = (row // 3) * 3
    col = (col // 3) * 3
    A = []
    k = -1
    for i in range(row, row + 3):
        A.append([])
        k += 1
        for j in range(col, col + 3):
            A[k].append(values[i][j])
    return A


def find_empty_positions(grid):
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j] == '.':
                return (i, j)
    return 0


def find_possible_values(grid, pos):
    A = {'1', '2', '3', '4', '5', '6', '7', '8', '9'}
    B = set(get_row(grid, pos))
    C = set(get_col(grid, pos))
    D = get_block(grid, pos)
    A -= B
    A -= C
    for i in range(3):
        A -= set(D[i])
    return A


def check_solution(solution):
    for i in range(9):
        A = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '.'}
        if A - set(get_row(solution, (i, 0))) != {'.'}:
            return False
        A = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '.'}
        if A - set(get_col(solution, (0, i))) != {'.'}:
            return False
    for i in ((0, 0), (0, 3), (0, 6), (3, 0), (3, 3), (3, 6)):
        A = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '.'}
        for e in get_block(solution, i):
            A -= set(e)
        if A != {'.'}:
            return False
    return True


def generate_sudoku(N):
    N -= 81
    grid = [['.' for i in range(9)] for i in range(9)]
    grid = solve(grid)
    emptyElem = [(i, j) for i in range(9) for j in range(9)]
    for k in range(N):
        remElem = random.choice(emptyElem)
        emptyElem.remove(remElem)
        grid[remElem[0]][remElem[1]] = '.'
    return grid


def solve(grid):
    pos = find_empty_positions(grid)
    if pos == 0:
        return grid
    val = list(find_possible_values(grid, pos))
    if not val:
        return grid
    if pos == 0:
        return None
    x, y = pos
    for e in val:
        grid[x][y] = e
        grid1 = solve(grid)
        if find_empty_positions(grid1) == 0:
            return grid1
    if grid1 == grid:
        grid[x][y] = '.'
    return grid1


if __name__ == '__main__':
    for fname in ['puzzle1.txt', 'puzzle2.txt', 'puzzle3.txt']:
        grid = read_sudoku(fname)
        display(grid)
        solution = solve(grid)
        display(solution)
        print(check_solution(solution))
