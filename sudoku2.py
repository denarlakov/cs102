# -*- coding: utf-8 -*-
import string
import random
import time


def read_sudoku(filename):
    digits = [c for c in open(filename).read() if c in '123456789.']
    grid = group(digits, 9)
    return grid


def display(values):
    width = 2
    line = '+'.join(['-' * (width * 3)] * 3)
    for row in range(9):
        print(''.join(values[row][col].center(width) + \
                      ('|' if str(col) in '25' else '') for col in range(9)))
        if str(row) in '25':
            print(line)
    print()


def group(values, n):
    grid = []
    previousrow = -1
    for i in range(0, len(values)):
        if i // n > previousrow:
            grid.append([])
            previousrow += 1
        grid[i // n].append(values[i])
    return grid


def get_row(grid, pos):
    row, col = pos
    i = 0
    list = []
    for j in grid:
        if i == row:
            list = j
        i += 1
    return list


def get_col(grid, pos):
    row, col = pos
    list = []
    for j in grid:
        for i in range(len(j)):
            if i == col:
                list.append(j[i])
    return list


def get_block(grid, pos):
    row, col = pos
    row = (row // 3) * 3
    col = (col // 3) * 3
    A = []
    k = -1
    for i in range(row, row + 3):
        for j in range(col, col + 3):
            A.append(grid[i][j])
    return A


def find_empty_positions(grid):
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j] == '.':
                return(i, j)


def find_possible_values(grid, pos):
    A = {'1', '2', '3', '4', '5', '6', '7', '8', '9'}
    B = set(get_row(grid, pos))
    C = set(get_col(grid, pos))
    D = get_block(grid, pos)
    A -= B
    A -= C
    for i in range(3):
        A -= set(D[i])
    return A


def solve(grid):
    t = find_empty_positions(grid)
    if t is None:
        return grid
    e = list(find_possible_values(grid, t))
    if e == []:
        return grid
    row, col = t
    for i in e:
        if find_empty_positions(grid) is None:
            return grid
        grid[row][col] = i
        grid1 = solve(grid)
        if find_empty_positions(grid1) is None:
            return grid1
    if grid1 == grid:
        grid[row][col] = '.'
    return grid1


def check_solution(solution):
    list1 = [0]*9
    t = True
    for i in solution:
        if t:
            for j in i:
                if j == '.':
                    t = False
                    break
                else:
                    list1[int(j) - 1] += 1
                    if list1[int(j)-1] > 9:
                        t = False
                        break
        else:
            break
    if t:
        return True
    else:
        return False

if __name__ == '__main__':
    for fname in ['puzzle1.txt', 'puzzle2.txt', 'puzzle3.txt']:
        grid = read_sudoku(fname)
        display(grid)
        solution = solve(grid)
        display(solution)
        print(check_solution(solution))
